import time
from pluto import Procedure, Step


class Procedure_test002(Procedure):

    def main(self):
        super().main()

        self.log("Hello World!")

        self.initiate_and_confirm_step(Step_smallDelay())

        self.log("Hello again.")


class Step_smallDelay(Step):

    def main(self):
        super().main()

        time.sleep(1)
