from datetime import timedelta
from pluto import Procedure


class Procedure_test002(Procedure):

    def preconditions(self):
        super().preconditions()
        self.wait_for(relative_time=timedelta(seconds=2))

    def main(self):
        super().main()
        self.log("Hello World!")
