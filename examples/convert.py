"""This file contains the function to convert PLUTO procedures to Python
scripts. Run it from the command line.
"""

import sys
from pluto import parser

if len(sys.argv) != 2:
    raise Exception("Please provide input file as argument")

with open(sys.argv[1]) as f:
    script = "\n".join(f.readlines()).strip()

# TODO: upon success, write output into <name>.py file
tree = parser.parse(script)
print(tree.pretty())
