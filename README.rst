Python-Pluto
============

Parse ECSS PLUTO scripts to Python code.

Getting Started
---------------

Clone the repository and prepare a virtual environment for Python3.

.. code:: bash

  git clone https://gitlab.com/librecube/prototypes/python-pluto
  cd python-pluto
  virtualenv -p python3 venv
  source venv/bin/activate
  pip install -r requirements.txt
  pip install -e .

Examples
--------

Go to the examples folder to try out to parse and run a procedure.

Parsing
~~~~~~~

To parse a PLUTO procedure to Python script:

.. code:: bash

  cd examples
  python convert.py test001.pluto

This will create a Python file of same name but with .py extension.

Running
~~~~~~~

To run a converted Python script:

.. code:: bash

  cd examples
  python run.py test001
