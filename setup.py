from setuptools import setup
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='python-pluto',
    version='0.0.1',
    description='Parse PLUTO scripts to Python',
    long_description=long_description,
    license='MIT',
    python_requires='>=3',
    keywords='ecss pluto dsl',
    packages=['pluto'],
    install_requires=[],
)
