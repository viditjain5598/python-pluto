from lark import Lark, Transformer


grammar = """
    start: instruction+
    instruction: repeat | log | declare | procedure | step | assignment | ifstatement | ifstatement1 | initialization
    procedure: "procedure" step* "end procedure"
    step: "initiate and confirm step" name activity "end step;"
    initialization: "initiate and confirm" name";"
    repeat: "repeat" NUMBER instruction
    log: "log" double_quoted* "+"* name* ";"
    STRING: /[a-zA-Z0-9_.-]{2,}/
    declare: "declare" variable* "end declare"
    variable: "variable" name "of type" type
    assignment: name ":=" double_quoted ";"
    ifstatement: "if value of" name COMPARISON name "then" activity "end if;"
    ifstatement1: "if" name COMPARISON name "then" activity "end if;"
    double_quoted: /"[^"]*"/
    COMPARISON: ">="|"<="|">"|"<"|"=="|"!="


    name: STRING
    type: STRING
    activity: instruction*
    %import common.INT -> NUMBER
    %import common.WS
    %ignore WS
    %ignore ","
"""

Parser = Lark(grammar)
