import threading
import time
from .constants import NOT_INITIATED, PRECONDITIONS, EXECUTING, \
    CONFIRMATION, COMPLETED, NOT_AVAILABLE, CONFIRMED, NOT_CONFIRMED, ABORTED


class Procedure:

    def __init__(self):
        self._execution_status = NOT_INITIATED
        self._confirmation_status = NOT_AVAILABLE

    def run(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs
        self._execution_status = NOT_INITIATED
        self._confirmation_status = NOT_AVAILABLE

        self.declaration()

        if self.preconditions() is False:
            self._execution_status = COMPLETED
            self._confirmation_status = ABORTED

        # create and start threads
        watchdog = threading.Thread(target=self.watchdog)
        main = threading.Thread(target=self.main)
        watchdog.start()
        main.start()

        # wait for threads to complete
        main.join()
        watchdog.join()

        if self.confirmation() is False:
            self._confirmation_status = NOT_CONFIRMED
        else:
            self._confirmation_status = CONFIRMED
        self._execution_status = COMPLETED

    """Procedure elements"""

    def declaration(self):
        # define here local events that can be
        # raised and received in this procedure
        pass

    def preconditions(self):
        """The preconditions body contains the conditions that define whether
        a procedure can start."""
        self._execution_status = PRECONDITIONS

    def main(self):
        self._execution_status = EXECUTING

    def watchdog(self):
        pass

    def confirmation(self):
        self._execution_status = CONFIRMATION

    """Procedure main statements"""

    def set_procedure_context(self, context):
        pass

    def initiate_in_parallel(self, *args):
        pass

    def initiate_and_confirm_step(self, step):
        # same logic as above for procedure running
        pass

    def initiate_and_confirm_activity(self, activity):
        pass

    def initiate_activity(self, activity):
        pass

    def inform_user(self, message):
        pass

    def log(self, message):
        # TODO: use logger module
        print(message)

    """Procedure property requests"""

    def get_executation_status(self):
        return self._execution_status

    def get_confirmation_status(self):
        return self._confirmation_status

    """Procedure operations"""

    def wait_for(self, relative_time=None, event_reference=None):
        if relative_time:
            time.sleep(relative_time.total_seconds())
