import threading
from .constants import NOT_INITIATED, PRECONDITIONS, EXECUTING, \
    CONFIRMATION, COMPLETED, NOT_AVAILABLE, CONFIRMED, NOT_CONFIRMED, ABORTED


class Step:

    def __init__(self):
        self._execution_status = NOT_INITIATED
        # ...

    def declaration(self):
        """Declare here local variables and local events."""
        raise NotImplementedError

    def precondition(self):
        self._execution_status = PRECONDITIONS
        return True

    def main(self):
        self._execution_status = EXECUTING
        # ...

    def watchdog(self):
        pass

    def confirmation(self):
        self._execution_status = CONFIRMATION
        return True

    """Operation requests"""

    def set_confirmation_status(self, status):
        self._confirmation_status = status

    """Property requests"""

    def get_execution_status(self):
        return self._execution_status

    def get_initiation_time(self):
        raise NotImplementedError

    def get_start_time(self):
        raise NotImplementedError

    def get_termination_time(self):
        raise NotImplementedError

    def get_confirmation_status(self):
        raise NotImplementedError

    def get_restart_number(self):
        raise NotImplementedError

    def get_completion_time(self):
        raise NotImplementedError
